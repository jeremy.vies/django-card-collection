# The django-card-collection

The django-card-collection is a web application to manage a card collection



# Quick start for local tests:
## Create the local venv
python3 -m venv $PWD/venv
source venv/bin/activate
pip3 install -r src/requirements.txt
## Start the django app
python src/manage.py migrate
export DJANGO_SUPERUSER_EMAIL=admin@example.com
export DJANGO_SUPERUSER_USERNAME=admin
export DJANGO_SUPERUSER_PASSWORD=admin
python src/manage.py createsuperuser --noinput
python src/manage.py runserver 0.0.0.0:8000
