"""
this module contains User Cards related views
"""

import random

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponseNotAllowed
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import CreateView, DetailView, DeleteView, FormView, UpdateView, ListView
from django.views.generic.edit import ProcessFormView
from django.views.generic.detail import SingleObjectMixin

from car_collection.models import CarModel, Brand, UserCollection, OwnedCar
from car_collection.forms import MyCarsAddForm, MyCarsGiveForm, MyCarsDeleteForm, MyCarsSellForm

class MyCarsView(LoginRequiredMixin, ListView):
    """ this view displays all owned cars """

    def get_template_names(self):
        return ['car_collection/mycars_all_'+self.view+'.html']

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.order_by = request.GET.get('order')
        self.view = request.GET.get('view', 'list')
        if self.view == 'list':
            self.paginate_by = request.user.settings.cars_per_page
        return super().get(request, args, kwargs)

    def get_queryset(self):
        if self.order_by is None:
            return self.user.collection.ownedcar_set.all().order_by('model__brand__name', 'model__name')
        elif self.order_by in ["model__name" , "-model__name"]:
            return self.user.collection.ownedcar_set.all().order_by('model__brand__name', self.order_by)
        else:
            return self.user.collection.ownedcar_set.all().order_by(self.order_by)

    def get_context_data(self, **kwargs):
        """ add to the context additional fields """

        # get the model from all owned_car of current user, and then count the distinct values
        nb_models = self.user.collection.ownedcar_set.all().values_list("model", flat=True).distinct().count()
        # count the number of all CarModel
        nb_models_total = CarModel.objects.all().count()

        context = super().get_context_data(nb_models=nb_models, nb_models_total=nb_models_total, **kwargs)
        return context

class MyCarsDetailView(LoginRequiredMixin, DetailView):
    """ this view displays details about a owned car """
    template_name = 'car_collection/mycars_detail.html'
    context_object_name = 'mycar'
    model = OwnedCar

    def get(self, request, *args, **kwargs):
        self.user = request.user
        # TODO check the id is part of user owned cars
        return super().get(request, args, kwargs)

class MyCarsAddView(LoginRequiredMixin, FormView):
    """ this view displays a list box to add to a user cars collection """
    template_name = "car_collection/mycars_add.html"
    form_class = MyCarsAddForm
    success_url = reverse_lazy("my_cars")

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.user = request.user
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        selected_carmodels = form.cleaned_data["selected_cars"]

        collection = self.user.collection
        for carmodel in selected_carmodels:
            ownedCar = OwnedCar(model=carmodel)
            ownedCar.collection = collection
            ownedCar.save()
        return super().form_valid(form)

class MyCarsDeleteView(LoginRequiredMixin, SingleObjectMixin, FormView):
    """ this view displays a form to delete several cars from collection """
    template_name = "car_collection/mycars_delete.html"
    form_class = MyCarsDeleteForm
    success_url = reverse_lazy("my_cars")
    model = OwnedCar

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['number'] = self.object.number
        return form_kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        number = form.cleaned_data["number"]

        if number <= self.object.number:
            self.object.number -= number
            self.object.save()

        if self.object.number == 0:
            self.object.delete()

        return super().form_valid(form)

def get_random_carmodel_id():
    """ return a random carmodel id """
    probas = {
        CarModel.Rank.E: 256,
        CarModel.Rank.D: 256,
        CarModel.Rank.C: 256,
        CarModel.Rank.B: 256,
        CarModel.Rank.A: 128,
        CarModel.Rank.S: 64,
        CarModel.Rank.SS: 32,
        CarModel.Rank.SSS: 16,
        CarModel.Rank.Z: 8,
        CarModel.Rank.ZZ: 4,
        CarModel.Rank.ZZZ: 2,
        CarModel.Rank.Uz: 1,
    }

    # compute the sum of all probas
    proba_sum = 0
    for _, value in probas.items():
        proba_sum += value
    print(f"proba_sum={proba_sum}")


    carmodel_index = -1

    # loop until we find rank with some cars
    while carmodel_index < 0:
        # get a random number and find in wich Rank it falls
        random_rank_index = random.randrange(1, proba_sum)
        range_max = 0
        index = None
        for key, value in probas.items():
            range_max += value
            if random_rank_index <= range_max:
                index = key
                break

        print(f"random_rank_index={random_rank_index}, index={index}")

        # get the queryset of this Rank
        qs = CarModel.objects.filter(rank=index)

        if (qs.count() > 0):
            carmodel_index = random.randrange(0, qs.count())
            print(f"carmodel_index={carmodel_index}, id={qs[carmodel_index].id}")
        else:
            print(f"no car in rank {index}")

    return qs[carmodel_index].id

class MyCarsAddRandomView(LoginRequiredMixin, DetailView):
    """ this view gets a randow car and display it. it also add it to our collection """
    template_name = 'car_collection/carmodel_detail.html'
    context_object_name = 'carmodel'
    model = CarModel

    def get_object(self, queryset=None):
        return CarModel.objects.all().filter(id=get_random_carmodel_id()).first()

    def get(self, request, *args, **kwargs):
        price = 120000

        self.user = request.user

        if self.user.collection.money >= price:
            self.user.collection.money -= price
            self.user.collection.save()

            result = super().get(request, *args, **kwargs)

            # save the car as a owned car in our collection
            ownedCar = OwnedCar.objects.filter(model=self.object, collection__user=self.user).first()
            if ownedCar is None:
                ownedCar = OwnedCar(model=self.object)
                ownedCar.collection = self.user.collection
            ownedCar.number += 1
            ownedCar.save()

            # print some message to inform its a new car
            messages.add_message(request, messages.INFO, f"Tu as une nouvelle Voiture {self.object.name} !!!")
            messages.add_message(request, messages.INFO, mark_safe(f"<a href=\"{reverse_lazy('my_cars')}\">Retour à la liste</a>"))
            return result
        else:
            messages.add_message(request, messages.ERROR, f"Tu n'as pas assez d'argent pour une nouvelle Voiture !!!")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

class MyCarsAddMultiRandomView(LoginRequiredMixin, ListView):
    """ this view displays all owned cars """

    def get_template_names(self):
        return ['car_collection/mycars_all_list.html']

    def get(self, request, *args, **kwargs):
        self.user =request.user
        price = 120000
        self.new_cars = []

        for i in range(0, 10):
            if self.user.collection.money >= price:
                self.user.collection.money -= price
                self.user.collection.save()

                id=get_random_carmodel_id()
                model = CarModel.objects.filter(id=get_random_carmodel_id()).first()
                ownedCar = OwnedCar.objects.filter(model=model, collection__user=self.user).first()
                if ownedCar is None:
                    ownedCar = OwnedCar(model=model)
                    ownedCar.collection = self.user.collection
                ownedCar.number += 1
                ownedCar.save()

                self.new_cars.append(ownedCar.id)
                # print some message to inform its a new car
                messages.add_message(request, messages.INFO, f"Tu as une nouvelle Voiture {ownedCar.model.name} !!!")
            else:
                messages.add_message(request, messages.ERROR, f"Tu n'as pas assez d'argent pour une nouvelle Voiture !!!")
                break

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.user.collection.ownedcar_set.filter(pk__in=self.new_cars)

class MyCarsGiveView(LoginRequiredMixin, SingleObjectMixin, FormView):
    """ this view displays a form to sell several cars from collection """
    template_name = "car_collection/mycars_give_car.html"
    form_class = MyCarsGiveForm
    success_url = reverse_lazy("my_cars")
    model = OwnedCar

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['number'] = self.object.number
        return form_kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        number = form.cleaned_data["number"]
        selected_user = form.cleaned_data["selected_user"]

        if number <= self.object.number:
            self.object.number -= number
            self.object.save()
            target_owned_car = selected_user.ownedcar_set.filter(model=self.object.model).first()
            if target_owned_car is None:
                target_owned_car = OwnedCar(model=self.object.model, collection=selected_user, number=0)
            target_owned_car.number += number
            target_owned_car.save()

        if self.object.number == 0:
            self.object.delete()

        return super().form_valid(form)

class MyCarsSellView(LoginRequiredMixin, SingleObjectMixin, FormView):
    """ this view displays a form to sell several cars from collection """
    template_name = "car_collection/mycars_sell.html"
    form_class = MyCarsSellForm
    success_url = reverse_lazy("my_cars")
    model = OwnedCar

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['number'] = self.object.number
        return form_kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.user = request.user
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        number = form.cleaned_data["number"]

        if number <= self.object.number:
            self.object.number -= number
            self.object.save()
            self.user.collection.money += self.object.model.price * number
            self.user.collection.save()

        if self.object.number == 0:
            self.object.delete()

        return super().form_valid(form)
