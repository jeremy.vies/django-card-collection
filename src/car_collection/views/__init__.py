"""
this module contains all views
"""
from .brand import *
from .carmodel import *
from .index import *
from .mycars import *
from .settings import *
