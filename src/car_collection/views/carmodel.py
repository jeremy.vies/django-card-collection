"""
this module contains Carmodel related views
"""
import os

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import CreateView, DetailView, DeleteView, UpdateView, ListView
from django.views.generic.list import MultipleObjectMixin
from django.shortcuts import render

from car_collection.models import CarModel, Brand, OwnedCar

# pylint: disable=R0901 # too many ancestor

class AllCarModelsView(LoginRequiredMixin, ListView):
    """ this view displays all car models """

    def get_template_names(self):
        return ['car_collection/carmodel_all_'+self.view+'.html']

    def get_queryset(self):
        if self.order_by is None:
            return CarModel.objects.all().order_by('brand__name', 'name')
        elif self.order_by in ["name" , "-name"]:
            return CarModel.objects.all().order_by('brand__name', self.order_by)
        else:
            return CarModel.objects.all().order_by(self.order_by)

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.order_by = request.GET.get('order')
        self.view = request.GET.get('view', 'list')
        if self.view == 'list':
            self.paginate_by = request.user.settings.cars_per_page
        return super().get(request, args, kwargs)

class NewCarModelView(LoginRequiredMixin, CreateView):
    """ create a new car """
    model = CarModel
    template_name = 'car_collection/carmodel_new.html'
    fields = ('name', 'brand', 'picture', 'year', 'description', 'max_speed', 'acceleration', 'horse_power', 'tork', 'weight', 'motor', 'event', 'rank', 'price')

    def get_initial(self):
        """ override get_initial to get default values from GET """
        brand_id = self.request.GET.get('brand', '')
        return {
          'brand': brand_id,
        }

    def get_success_url(self):
        """ override get_success_url() to return to the parent brand page """
        return reverse_lazy('brand', args=(self.object.brand.pk,))

class CarModelView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    """ this view provides details about the Car provided by its pk """
    template_name = 'car_collection/carmodel_detail.html'
    context_object_name = 'carmodel'
    model = CarModel

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, args, kwargs)

    def get_context_data(self, **kwargs):
        object_list = CarModel.objects.all().order_by('name')
        context = super().get_context_data(object_list=object_list, **kwargs)

        # count the number of owned_car from user.collection and store it into the context
        owned_car = self.user.collection.ownedcar_set.filter(model=self.object).first()
        if owned_car is not None:
            context['owned_count'] = owned_car.number
        else:
            context['owned_count'] = 0
        # count the number of owned_car from every own
        context['total_owned_count'] = 0
        for item in OwnedCar.objects.filter(model=self.object):
            context['total_owned_count'] = context['total_owned_count'] + item.number

        return context

class EditCarModelView(LoginRequiredMixin, UpdateView):
    """ edit a Car """
    model = CarModel
    template_name = 'car_collection/carmodel_edit.html'
    success_url = reverse_lazy('carmodel_all')
    fields = ('name', 'brand', 'picture', 'year', 'description', 'max_speed', 'acceleration', 'horse_power', 'tork', 'weight', 'motor', 'event', 'rank', 'price')

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

class DeleteCarModelView(LoginRequiredMixin, DeleteView):
    """ this view requests the deletion of a car """
    model = CarModel
    template_name = 'car_collection/carmodel_delete.html'

    def get_success_url(self):
        """ override get_success_url() to return to the collection page """
        return reverse_lazy('brand', args=(self.object.brand.pk,))
