
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import View, FormView

from car_collection.forms import MoneyGiveForm
from car_collection.views import AllBrandsView

# pylint: disable=R0901 # too many ancestor

class IndexView(AllBrandsView):
    pass

class AddMoneyView(LoginRequiredMixin, View):
    """ simple view to add a small amount of money """

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.user.collection.money += 50
        self.user.collection.save()
        return HttpResponseRedirect(reverse_lazy('index'))

class GiveMoneyView(LoginRequiredMixin, FormView):
    """ view to give an amount of money to another player """
    template_name = 'car_collection/money_give.html'
    form_class = MoneyGiveForm

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.user = request.user
        self.next = request.POST.get('next', '/')
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):

        amount = form.cleaned_data["amount"]
        selected_user = form.cleaned_data["selected_user"]

        # check it is not the same user and we have enough money
        if self.user.id != selected_user.id and \
                self.user.collection.money >= amount:
            self.user.collection.money -= amount
            self.user.collection.save()

            selected_user.money += amount
            selected_user.save()

        return HttpResponseRedirect(self.next)
