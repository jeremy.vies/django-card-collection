"""
this module contains settings related views
"""

from django.http import HttpResponseRedirect
from django.views.generic import UpdateView
from django.urls import reverse_lazy

from car_collection.models import UserSettings

class EditSettingsView(UpdateView):
    """ this is the update view for Settings """
    model = UserSettings
    fields = ['cars_per_page', ]
    template_name = 'car_collection/edit_settings.html'
    success_url = reverse_lazy('index')

    def get(self, request, *args, **kwargs):
        """ override get() to catch request.user """
        self.user = request.user
        return super().get(request, args, kwargs)

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        self.user = request.user
        return super().post(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            if self.user.settings is not None:
                return self.user.settings
        except UserSettings.DoesNotExist:
            pass
        new = UserSettings.objects.create()
        self.user.settings = new
        return self.user.settings

    def get_success_url(self):
        """ override get_success_url() to return to the all repositories page """
        return reverse_lazy('index')
