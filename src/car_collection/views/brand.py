"""
this module provides Collection related views
"""

import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView, ListView
from django.views.generic.list import MultipleObjectMixin

from django_q.tasks import async_task
from django_q.models import Schedule

from car_collection.models import Brand

# pylint: disable=R0901 # too many ancestor

class AllBrandsView(LoginRequiredMixin, ListView):
    """ this view displays all collections """
    template_name = 'car_collection/brand_all.html'

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.paginate_by = request.user.settings.cars_per_page
        return super().get(request, args, kwargs)

    def get_queryset(self):
        return Brand.objects.all().order_by('name')

class BrandView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    """ this view provides details about a brand """
    template_name = 'car_collection/brand.html'
    context_object_name = 'brand'
    model = Brand

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.paginate_by = request.user.settings.cars_per_page
        return super().get(request, args, kwargs)

    def get_context_data(self, **kwargs):
        object_list = self.object.carmodel_set.order_by('name')
        context = super().get_context_data(object_list=object_list, **kwargs)
        return context

class NewBrandView(LoginRequiredMixin, CreateView):
    """ create a new brand """
    model = Brand
    template_name = 'car_collection/brand_new.html'
    fields = ('name', 'logo')
    success_url = reverse_lazy('brand_all')

class EditBrandView(LoginRequiredMixin, UpdateView):
    """ edit a collebrandction """
    model = Brand
    template_name = 'car_collection/brand_edit.html'
    fields = ('name', 'logo')
    success_url = reverse_lazy('brand_all')

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

class DeleteBrandView(LoginRequiredMixin, DeleteView):
    """ this view requests the deletion of an brand """
    model = Brand
    template_name = 'car_collection/brand_delete.html'
    success_url = reverse_lazy('brand_all')
