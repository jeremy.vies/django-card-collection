from django import forms

from car_collection.models import CarModel, UserCollection

class MyCarsAddForm(forms.Form):
    selected_cars = forms.ModelMultipleChoiceField(queryset=CarModel.objects.all())

class MyCarsDeleteForm(forms.Form):
    number = forms.IntegerField(min_value=0)
    def __init__(self, *args, **kwargs):
        # Pop the number argument, as the parent form doesn't know about it!
        number = kwargs.pop("number")
        super().__init__(*args, **kwargs)
        self.fields['number'].widget.attrs['max'] = number

class MyCarsSellForm(forms.Form):
    number = forms.IntegerField(min_value=0)
    def __init__(self, *args, **kwargs):
        # Pop the number argument, as the parent form doesn't know about it!
        number = kwargs.pop("number")
        super().__init__(*args, **kwargs)
        self.fields['number'].widget.attrs['max'] = number

class MyCarsGiveForm(forms.Form):
    selected_user = forms.ModelChoiceField(queryset=UserCollection.objects.all())
    number = forms.IntegerField(min_value=0)
    def __init__(self, *args, **kwargs):
        # Pop the number argument, as the parent form doesn't know about it!
        number = kwargs.pop("number")
        super().__init__(*args, **kwargs)
        self.fields['number'].widget.attrs['max'] = number

class MoneyGiveForm(forms.Form):
    selected_user = forms.ModelChoiceField(queryset=UserCollection.objects.all())
    amount = forms.IntegerField(min_value=0)
