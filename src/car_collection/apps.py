from django.apps import AppConfig


class CarCollectionConfig(AppConfig):
    name = 'car_collection'
