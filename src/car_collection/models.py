"""
this module contains all Models
"""

import uuid

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from . import settings as app_settings


class Brand(models.Model):
    """ a car brand """

    name = models.CharField(max_length=512)
    logo = models.ImageField(upload_to='logo', blank=True)

    def __str__(self):
        return self.name

class CarModel(models.Model):
    """ a car """

    class Rank(models.TextChoices):
        E = 'E'
        D = 'D'
        C = 'C'
        B = 'B'
        A = 'A'
        S = 'S'
        SS = 'SS'
        SSS = 'SSS'
        Z = 'Z'
        ZZ = 'ZZ'
        ZZZ = 'ZZZ'
        Uz = 'Uz'

        @classmethod
        def value_from_label(cls, label:str):
            index = 0
            for l in cls.labels:
                if label == l:
                    return cls.values[index]
                index += 1
            raise ValueError()

    name = models.CharField(max_length=512)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to='cars')
    description = models.TextField(default="", blank=True)
    # description details
    max_speed = models.CharField(max_length=16, default="", blank=True)
    acceleration = models.CharField(max_length=16, default="", blank=True)
    horse_power = models.CharField(max_length=16, default="", blank=True)
    tork = models.CharField(max_length=16, default="", blank=True)
    weight = models.CharField(max_length=16, default="", blank=True)
    motor = models.CharField(max_length=16, default="", blank=True)
    #
    event = models.TextField(default="", blank=True)
    year = models.IntegerField(default=2000)
    rank = models.CharField(
        max_length=3, choices=Rank.choices, default=Rank.A)
    price = models.IntegerField(default=0)


    def __str__(self):
        return self.name

    @property
    def rank_url(self):
        return f"car_collection/rangs/Rang_{self.rank}.webp"

class UserCollection(models.Model):
    """ all the cars owned by a user """

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='collection', null=True)
    money = models.FloatField(default=100)

    def __str__(self):
        return "Collection de "+self.user.username

class OwnedCar(models.Model):
    """ a car owned by someone"""

    model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    collection = models.ForeignKey(UserCollection, on_delete=models.CASCADE, null=True)
    number = models.IntegerField(default=0)

class UserSettings(models.Model):
    """ app settings per user """

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='settings', null=True)
    cars_per_page = models.IntegerField(default=10)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_settings(sender, instance, created, **kwargs):
    """" signal receiver to create a UserSettings object upon User creation """

    if created:
        UserSettings.objects.create(user=instance)
        UserCollection.objects.create(user=instance)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def save_user_settings(sender, instance, **kwargs):
    """ signal to save the UserSettings object upon User save """

    instance.settings.save()
    instance.collection.save()
