from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('brands/', views.AllBrandsView.as_view(), name='brand_all'),
    path('brand/new/', views.NewBrandView.as_view(), name='brand_new'),
    path('brand/<str:pk>/', views.BrandView.as_view(), name='brand'),
    path('brand/<str:pk>/edit/', views.EditBrandView.as_view(), name='brand_edit'),
    path('brand/<str:pk>/delete/', views.DeleteBrandView.as_view(), name='brand_delete'),
    path('carmodels/', views.AllCarModelsView.as_view(), name='carmodel_all'),
    path('carmodel/new/', views.NewCarModelView.as_view(), name='carmodel_new'),
    path('carmodel/<str:pk>/', views.CarModelView.as_view(), name='carmodel'),
    path('carmodel/<str:pk>/edit/', views.EditCarModelView.as_view(), name='carmodel_edit'),
    path('carmodel/<str:pk>/delete/', views.DeleteCarModelView.as_view(), name='carmodel_delete'),
    path('mycars/', views.MyCarsView.as_view(), name='my_cars'),
    path('mycars/add/', views.MyCarsAddView.as_view(), name='my_cars_add_cars'),
    path('mycars/add_random/', views.MyCarsAddRandomView.as_view(), name='my_cars_add_random_car'),
    path('mycars/add_multirandom/', views.MyCarsAddMultiRandomView.as_view(), name='my_cars_add_multi_random_car'),
    path('mycars/<str:pk>/', views.MyCarsDetailView.as_view(), name='my_cars_detail'),
    path('mycars/<str:pk>/delete/', views.MyCarsDeleteView.as_view(), name='my_cars_delete'),
    path('mycars/<str:pk>/give/', views.MyCarsGiveView.as_view(), name='my_cars_give'),
    path('mycars/<str:pk>/sell/', views.MyCarsSellView.as_view(), name='my_cars_sell'),
    path('settings/edit', views.EditSettingsView.as_view(), name='edit_settings'),
    path('add_money', views.AddMoneyView.as_view(), name='add_money'),
    path('give_money', views.GiveMoneyView.as_view(), name='give_money'),
]

if settings.DEBUG:
     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
