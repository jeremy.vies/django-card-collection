# Generated by Django 3.2.8 on 2023-10-22 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car_collection', '0004_auto_20231022_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carmodel',
            name='description',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='carmodel',
            name='event',
            field=models.TextField(blank=True, default=''),
        ),
    ]
