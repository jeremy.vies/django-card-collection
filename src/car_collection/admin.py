from django.contrib import admin

# Register your models here.

from .models import CarModel, Brand, OwnedCar, UserCollection, UserSettings

admin.site.register(CarModel)
admin.site.register(Brand)
admin.site.register(OwnedCar)
admin.site.register(UserCollection)
admin.site.register(UserSettings)
